CFLAGS=-std=c11 -O2 -Wall -Werror -pedantic-errors -MMD -I./
LDFLAGS=-lrt -lm

LIBEVENT_CFLAGS  = $(shell pkg-config --cflags libevent)
LIBEVENT_LDFLAGS = $(shell pkg-config --libs   libevent)

LIBASOUND_CFLAGS  =
LIBASOUND_LDFLAGS = -lasound

LIBMNL_CFLAGS  = $(shell pkg-config --cflags libmnl)
LIBMNL_LDFLAGS = $(shell pkg-config --libs   libmnl)

LIBX11_CFLAGS  =
LIBX11_LDFLAGS = -lX11

CFLAGS  += $(LIBEVENT_CFLAGS)
LDFLAGS += $(LIBEVENT_LDFLAGS) $(LIBASOUND_LDFLAGS) $(LIBMNL_LDFLAGS) \
	$(LIBX11_LDFLAGS)

RM = rm -f

SRC = $(wildcard *.c common/*.c widget/*.c)
OBJ = $(patsubst %.c,%.o,$(SRC))
DEP = $(patsubst %.o,%.d,$(OBJ))
PROGRAM = async-dwm-st

.PHONY: all
$(PROGRAM) all: $(OBJ)

common/error_func.o widget/datetime.o widget/alsa.o widget/cmd.o:\
  CFLAGS+=-D_GNU_SOURCE

common/widget.o: CFLAGS+=$(LIBX11_CFLAGS)

widget/alsa.o: CFLAGS+=$(LIBASOUND_CFLAGS)

widget/essid.o: CFLAGS+=$(LIBMNL_CFLAGS)

-include $(DEP)

.PHONY: clean
clean:
	$(RM) $(OBJ)
	$(RM) $(DEP)
	$(RM) $(PROGRAM)

