#include <stdlib.h>

#include <event2/event.h>

#include "config.h"
#include "common.h"

#define widgets_size sizeof(widgets)/sizeof(widget_t)

static void init(int argc, char **argv);
static void cleanup();

int main(int argc, char **argv) {
    init(argc, argv);
    common_event_dispatch();
    cleanup();
    return EXIT_SUCCESS;
}

static void init(int argc, char **argv) {
    common_event_init();
    common_widget_init(widgets, widgets_size);
}

static void cleanup() {
    common_widget_cleanup();
    common_event_cleanup();
}
