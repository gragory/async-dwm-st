#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>

#define BUF_SIZE 1024

static void errmsg(
    bool use_err, int err, const char *prefix, const char *format, va_list va
) {
    if (prefix != NULL) {
        fputs(prefix, stderr);
        fputs(": ", stderr);
    }
    vfprintf(stderr, format, va);
    if (use_err) {
        char errstr[BUF_SIZE];
        fputs(": ",   stderr);
        fputs(strerror_r(err, errstr, BUF_SIZE), stderr);
    }
    fputc('\n', stderr);
    fflush(stderr);
}

void _warn(const char *prefix, const char *format, ...) {
    va_list va;
    va_start(va, format);
    errmsg(false, 0, prefix, format, va);
    va_end(va);
}

void _vwarn(const char *prefix, const char *format, va_list va) {
    errmsg(false, 0, prefix, format, va);
}

void _warn_err(const char *prefix, const char *format, ...) {
    va_list va;
    int saved_errno;
    saved_errno = errno;
    va_start(va, format);
    errmsg(true, errno, prefix, format, va);
    va_end(va);
    errno = saved_errno;
}

noreturn void _die(const char *prefix, const char *format, ...) {
    va_list va;
    va_start(va, format);
    errmsg(false, 0, prefix, format, va);
    va_end(va);
    _Exit(EXIT_FAILURE);
}

noreturn void _die_err(const char *prefix, const char *format, ...) {
    va_list va;
    va_start(va, format);
    errmsg(true, errno, prefix, format, va);
    va_end(va);
    _Exit(EXIT_FAILURE);
}
