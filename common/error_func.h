#ifndef COMMON_ERROR_FUNC_H
#define COMMON_ERROR_FUNC_H

#include <stdnoreturn.h>
#include <stdlib.h>
#include <stdarg.h>

#ifndef ERROR_PREFIX
#define ERROR_PREFIX NULL
#endif

#define warn(...)     _warn(ERROR_PREFIX, __VA_ARGS__)
#define vwarn(...)    _vwarn(ERROR_PREFIX, __VA_ARGS__)
#define warn_err(...) _warn_err(ERROR_PREFIX, __VA_ARGS__)
#define die(...)      _die(ERROR_PREFIX, __VA_ARGS__)
#define die_err(...)  _die_err(ERROR_PREFIX, __VA_ARGS__)

void _warn(const char *prefix, const char *format, ...);

void _vwarn(const char *prefix, const char *format, va_list va);

void _warn_err(const char *prefix, const char *format, ...);

noreturn void _die(const char *prefix, const char *format, ...);

noreturn void _die_err(const char *prefix, const char *format, ...);

#endif /* COMMON_ERROR_FUNC_H */

