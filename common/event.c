#include <signal.h>
#include <stdlib.h>
#include <stdnoreturn.h>

#include <event2/event.h>

#define ERROR_PREFIX "libevent"

#include "common/error_func.h"
#include "common/event.h"

struct sigevent_s {
    int sig;
    struct event *ev;
};

static struct event_base *evbase = NULL;

static struct sigevent_s sigevent[] = {
    { .sig = SIGINT,  .ev = NULL },
    { .sig = SIGTERM, .ev = NULL },
};

#define sigevent_size sizeof(sigevent)/sizeof(struct sigevent_s)

static void libevent_log_cb(int severity, const char *msg) {
    switch(severity) {
        case EVENT_LOG_WARN:
            warn("warn: %s", msg);
            break;
        case EVENT_LOG_ERR:
            warn("error: %s", msg);
            break;
        default:
            break;
    }
}

static noreturn void fatal_libevent_cb(int err) {
    die("fatal: %d", err);
}

static void sig_cb(evutil_socket_t fd, short what, void *arg) {
    int sig = *((int *) arg);
    switch(sig) {
        case SIGTERM:
        case SIGINT:
            event_base_loopbreak(evbase);
            break;
        default:
            break;
    }
}

void common_event_init() {
    event_set_fatal_callback(fatal_libevent_cb);
    event_set_log_callback(libevent_log_cb);
    evbase = event_base_new();
    for (int i = 0; i < sigevent_size; i++) {
        sigevent[i].ev = evsignal_new(
            evbase, sigevent[i].sig, sig_cb, (void *) &(sigevent[i].sig)
        );
        if (sigevent[i].ev == NULL)
            die_err("evsignal_new");
        if (event_add(sigevent[i].ev, NULL) != 0)
            die_err("event_add");
    }
}

void common_event_dispatch() {
    event_base_dispatch(evbase);
}

void common_event_cleanup() {
    for (int i = 0; i < sigevent_size; i++) {
        if (sigevent[i].ev != NULL)
            event_free(sigevent[i].ev);
    }
    libevent_global_shutdown();
}

struct event *common_event_new(
    evutil_socket_t fd, short what, event_callback_fn cb, void *arg
) {
    return event_new(evbase, fd, what, cb, arg);
}
