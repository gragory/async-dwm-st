#ifndef COMMON_EVENT_H
#define COMMON_EVENT_H

#include <event2/event.h>

void common_event_init();
void common_event_dispatch();
void common_event_cleanup();

struct event *common_event_new(
    evutil_socket_t fd, short what, event_callback_fn cb, void *arg
);

#define common_sigevent_new(sig, cb, arg) \
    common_event_new(sig, EV_SIGNAL|EV_PERSIST, cb, arg)

#endif /* COMMON_EVENT_H */
