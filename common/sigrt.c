#include <stdbool.h>
#include <signal.h>
#include <stdlib.h>
#include <assert.h>

static bool sigs[6];

int common_sigrt_new() {
    assert(SIGRTMAX-SIGRTMIN >= sizeof(sigs));
    for (size_t i = 0; i < sizeof(sigs); i++) {
        if (sigs[i] == false) {
            sigs[i] = true;
            return SIGRTMIN + i;
        }
    }
    return -1;
}

void common_sigrt_free(int sig) {
    sig -= SIGRTMIN;
    if (sig < 0 || sig >= sizeof(sig))
        return;
    sigs[sig] = false;
}
