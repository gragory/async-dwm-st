#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <event2/event.h>
#include <X11/Xlib.h>

#define ERROR_PREFIX "widget"

#include "common/widget.h"
#include "common/error_func.h"
#include "common/event.h"

static widget_t *widgets   = NULL;
static size_t widgets_size = 0;

Display *dpy   = NULL;
char *st_buf   = NULL;
size_t st_size = 0;


#define foreach_widget(w) \
    for (size_t i=0; i<widgets_size; i++) \
        if (((w) = widgets + i) != NULL) \

/* externs from config.h */
extern const char *error_st;
extern const char *na_st;
extern const bool  debug_stdout;

static inline void cleanup_one(widget_t *w) {
    if (w->status == WIDGET_UNINITED)
        return;
    if (w->cleanup != NULL)
        w->cleanup(w);
    if (w->ev != NULL)
        event_free(w->ev);
    if (w->result.buf != NULL)
        free(w->result.buf);
    w->status = WIDGET_UNINITED;
}

void common_widget_init(widget_t *_widgets, size_t _size) {
    widgets = _widgets;
    widgets_size = _size;
    dpy = XOpenDisplay(NULL);
    if (dpy == NULL)
        die("XOpenDisplay failed");
    widget_t *w;
    foreach_widget(w) {
        w->status = WIDGET_UNINITED;
        w->result.buf = malloc(sizeof(char) * w->result.size);
        st_size += w->result.size > 16 ?  w->result.size : 16;
        if (w->result.buf == NULL) {
            warn_err("malloc");
            continue;
        }
        /* init must change status if initialization comleted */
        w->result.buf[0] = '\0';
        w->status = WIDGET_ERROR;
        w->init(w);
        if (w->status == WIDGET_ERROR)
            cleanup_one(w);
    }

    st_buf = malloc(sizeof(char) * st_size);
    if (st_buf == NULL)
        die_err("malloc");

    common_widget_update_st();
}

void common_widget_cleanup() {
    widget_t *w;
    XCloseDisplay(dpy);
    free(st_buf);
    foreach_widget(w)
        cleanup_one(w);
}

void common_widget_update_st() {
    widget_t *w;
    if (debug_stdout) {
        foreach_widget(w) {
            switch(w->status) {
                case WIDGET_OK:
                    fputs(w->result.buf, stdout);
                    break;
                case WIDGET_ERROR:
                case WIDGET_UNINITED:
                    fputs(error_st, stdout);
                    break;
                case WIDGET_NA:
                default:
                    fputs(na_st, stdout);
                    break;
            }
        }
        fputc('\n', stdout);
    }
    else {
        st_buf[0] = '\0';
        foreach_widget(w) {
            switch(w->status) {
                case WIDGET_OK:
                    strcat(st_buf, w->result.buf);
                    break;
                case WIDGET_ERROR:
                case WIDGET_UNINITED:
                    strcat(st_buf, error_st);
                    break;
                case WIDGET_NA:
                default:
                    strcat(st_buf, na_st);
                    break;
            }
        }
        XStoreName(dpy, DefaultRootWindow(dpy), st_buf);
        XSync(dpy, False);
    }
}

