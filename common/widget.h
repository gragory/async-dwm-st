#ifndef COMMON_WIDGET_H
#define COMMON_WIDGET_H

#include <stdlib.h>

#include <event2/event.h>

enum wstatus_e {
    WIDGET_OK, WIDGET_ERROR, WIDGET_UNINITED, WIDGET_NA
};

struct widget_s {
    enum wstatus_e  status;
    const char     *fmt;
    struct event   *ev;
    void           *data;

    void (*init)(struct widget_s *);
    void (*cleanup)(struct widget_s *);

    struct { char *buf; size_t size; } result;
};

typedef struct widget_s widget_t;
typedef enum wstatus_e  wstatus_t;

#define COMMON_WIDGET(bufsize) \
    .status  = WIDGET_UNINITED, \
    .fmt     = NULL,      \
    .ev      = NULL,      \
    .data    = NULL,      \
    .init    = NULL,      \
    .cleanup = NULL,      \
    .result  = { .buf = NULL, .size = (bufsize) } \

void common_widget_init(widget_t *widgets, size_t size);
void common_widget_cleanup();
void common_widget_update_st();

#endif /* COMMON_WIDGET_H */
