#include <stdbool.h>
#include "widget.h"

const char *error_st     = "[ ERROR ]";
const char *na_st        = "[ N/A ]";
const bool  debug_stdout = false;

alsa_args_t alsa_args = {
    .card    = "default",
    .channel = "Master",
};

#define BATTERY "BAT0"
#define BETTERY_PATH_CAPACITY "/sys/class/power_supply/"BATTERY"/capacity"
#define BATTERY_PATH_STATUS   "/sys/class/power_supply/"BATTERY"/status"

battery_args_t battery_args = {
    .delay = 15,
    .path_capacity = BETTERY_PATH_CAPACITY,
    .path_status   = BATTERY_PATH_STATUS,
};


//char *cmd[] = NEW_SHELL_CMD("echo 123");
char *xkbd_cmd[] = NEW_SHELL_CMD("xset -q "\
    "| awk '/Group 2/{print($4==\"off\"?\"en\":\"ru\")}'"
);
cmd_args_t xkbd_args = {
    .delay   = 1,
    .timeout = 10,
    .cmd     = xkbd_cmd,
};

widget_t widgets[] = {
    /* WIDGET_XXX(fmt, bufsize, extra_data) */
    WIDGET_ESSID   ("[%s]",               32, "wlp1s0"),
    WIDGET_CMD     ("[\xE2\x8C\xA8 %s]",  16, &xkbd_args),
    WIDGET_BATTERY ("[\xE2\x9A\xA1%5s]",  16, &battery_args),
    WIDGET_ALSA    ("[\xE2\x99\xAB %4s]", 16, &alsa_args),
    WIDGET_DATETIME("[ %d.%m.%Y %H:%M ]", 32, 60),
};

// netlink for interface changes
