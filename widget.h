#ifndef WIDGET_H
#define WIDGET_H

#include "common/widget.h"
#include "widget/alsa.h"
#include "widget/datetime.h"
#include "widget/battery.h"
#include "widget/cmd.h"
#include "widget/essid.h"

#endif /* WIDGET_H */
