#include <math.h>
#include <poll.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <alsa/asoundlib.h>
#include <event2/event.h>

#define ERROR_PREFIX "wdgt-alsa"
#include "common.h"

#include "widget/alsa.h"

/* 10^x = e^(log(10^x)) = e^(x*log(10)) */
#define exp10(x) (exp((x) * log(10)))

#define MAX_LINEAR_DB_SCALE 24

#define VOL_NA   -1.0
#define VOL_MUTE -2.0

typedef struct alsa_data_s {
    bool                  mixer_allocated;
    bool                  sid_allocated;
    snd_mixer_t          *mixer;
    snd_mixer_selem_id_t *sid;
    snd_mixer_elem_t     *elem;
} alsa_data_t;

static void alsa_error_cb (
    const char *file, int line, const char *function,
    int err, const char *fmt, ...
) {
    va_list va;
    va_start(va, fmt);
    vwarn(fmt, va);
    va_end(va);
}

static double volume_perc(snd_mixer_elem_t *elem) {
    int err = 0;
#define alsa_call(func, ...) \
    if ((err = func(__VA_ARGS__)) < 0) { \
        warn(#func": %s", snd_strerror(err)); \
        return VOL_NA; \
    }
    int pswitch;
    alsa_call(snd_mixer_selem_get_playback_switch, elem, 0, &pswitch);
    if (!pswitch)
        return VOL_MUTE;

    long max, min, cur;
    alsa_call(snd_mixer_selem_get_playback_dB_range, elem, &min, &max);
    if (min >= max) {
        alsa_call(snd_mixer_selem_get_playback_volume_range, elem, &min, &max);
        if (min == max) {
            warn("snd_mixer_selem_get_playback_volume_range: min == max");
            return VOL_NA;
        }
        alsa_call(snd_mixer_selem_get_playback_volume, elem, 0, &cur);
        return (double)(cur - min) / (double)(max - min) * 100.0;
    }

    alsa_call(snd_mixer_selem_get_playback_dB, elem, 0, &cur);
    if (max - min <= MAX_LINEAR_DB_SCALE * 100)
        return (double)(cur - min) / (double)(max - min) * 100.0;

    double normalized, min_norm;
    normalized = exp10((cur - max) / 6000.0);
    if (min != SND_CTL_TLV_DB_GAIN_MUTE) {
        min_norm = exp10((min - max) / 6000.0);
        normalized = (normalized - min_norm) / (1 - min_norm);
    }
    return normalized * 100.0;
#undef alsa_call
}

static wstatus_t update(widget_t *self) {
    alsa_data_t *data = (alsa_data_t *)self->data;
    double vol = volume_perc(data->elem);
    if (vol == VOL_NA) {
        return self->status = WIDGET_NA;
    }
    char ready[5];
    if (vol == VOL_MUTE) {
        strncpy(ready, "mute", sizeof(ready));
    }
    else {
        snprintf(ready, sizeof(ready), "%.0f%%", vol);
    }
    ready[4] = '\0';
    snprintf(self->result.buf, self->result.size, self->fmt, ready);
    self->result.buf[self->result.size-1] = '\0';
    return self->status = WIDGET_OK;
}

static void event_cb(evutil_socket_t fd, short what, void *arg) {
    widget_t    *self = (widget_t *)arg;
    alsa_data_t *data = (alsa_data_t *)self->data;
    snd_mixer_handle_events(data->mixer);
    update(self);
    common_widget_update_st();
}

void alsa_init(widget_t *self) {
    static bool first_call = false;
    if (!first_call) {
        snd_lib_error_set_handler(alsa_error_cb);
        first_call = true;
    }

    alsa_args_t *args = (alsa_args_t *)self->data;
    alsa_data_t *data = self->data = malloc(sizeof(alsa_data_t));
    if (data == NULL) {
        warn_err("malloc");
        return;
    }
    data->mixer = NULL;
    data->sid   = NULL;
    data->elem  = NULL;
    data->mixer_allocated = data->sid_allocated = false;

    int err = 0;
#define alsa_call(func, ...) \
    if ((err = func(__VA_ARGS__)) < 0) { \
        warn(#func": %s", snd_strerror(err)); \
        return; \
    }
    alsa_call(snd_mixer_open, &data->mixer, 0);
    data->mixer_allocated = true;
    alsa_call(snd_mixer_attach, data->mixer, args->card);
    alsa_call(snd_mixer_selem_register, data->mixer, NULL, NULL);
    alsa_call(snd_mixer_load, data->mixer)
    alsa_call(snd_mixer_selem_id_malloc, &data->sid);
    data->sid_allocated = true;
#undef alsa_call

    snd_mixer_selem_id_set_name(data->sid, args->channel);
    data->elem = snd_mixer_find_selem(data->mixer, data->sid);
    if (data->elem == NULL) {
        warn("channel '%s' not found", args->channel);
        return;
    }

    if (snd_mixer_poll_descriptors_count(data->mixer) < 1) {
        warn("no poll descriptors found");
        return;
    }

    struct pollfd pfd[1];
    snd_mixer_poll_descriptors(data->mixer, pfd, 1);

    self->ev = common_event_new(
        pfd[0].fd, EV_READ|EV_PERSIST, event_cb, (void *)self
    );
    if (self->ev == NULL) {
        warn_err("event_new");
        return;
    }
    if (event_add(self->ev, NULL) != 0) {
        warn_err("event_add");
        return;
    }

    update(self);
}

void alsa_cleanup(widget_t *self) {
    if (self->data == NULL)
        return;
    alsa_data_t *data = (alsa_data_t *)self->data;
    if (data->sid_allocated)
        snd_mixer_selem_id_free(data->sid);
    if (data->mixer_allocated)
        snd_mixer_close(data->mixer);
    free(self->data);
}
