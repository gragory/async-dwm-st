#ifndef WIDGET_ALSA_H
#define WIDGET_ALSA_H

#include "common/widget.h"

struct alsa_args_s {
    const char *card;
    const char *channel;
};

typedef struct alsa_args_s alsa_args_t;

#define WIDGET_ALSA(format, bufsize, alsa_args) { \
        COMMON_WIDGET(bufsize),    \
        .init     = alsa_init,     \
        .cleanup  = alsa_cleanup,  \
        .fmt      = (format),      \
        .data     = (void *)(alsa_args), \
    }

void alsa_init(widget_t *self);
void alsa_cleanup(widget_t *self);

#endif /* WIDGET_ALSA_H */
