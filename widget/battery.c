#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <event2/event.h>

#define ERROR_PREFIX "wdgt-battery"
#include "common.h"

#include "widget/battery.h"

enum bstatus_e {
    BATTERY_CHARGING,
    BATTERY_DISCHARGING,
    BATTERY_FULL,
    BATTERY_UNKNOWN,
};

int bstatus2ch[] = {
    [BATTERY_CHARGING]    = '+',
    [BATTERY_DISCHARGING] = '-',
    [BATTERY_FULL]        = '=',
    [BATTERY_UNKNOWN]     = '?',
};

typedef struct battery_data_s {
    int             perc;
    enum bstatus_e  status;
    bool            notchanged;
    const char     *path_capacity;
    const char     *path_status;
} battery_data_t;

enum bstatus_e battery_status(const char *path) {
    FILE *fp = fopen(path, "r");
    if (fp == NULL) {
        warn_err("fopen '%s'", path);
        return BATTERY_UNKNOWN;
    }

    char state[12];
    int  res = fscanf(fp, "%12s", state);
    fclose(fp);
    if (res != 1) {
        warn("failed to parse '%s'", path);
        return BATTERY_UNKNOWN;
    }

    if (strcmp(state, "Charging") == 0)
        return BATTERY_CHARGING;
    else if (strcmp(state, "Discharging") == 0)
        return BATTERY_DISCHARGING;
    else if (strcmp(state, "Full") == 0)
        return BATTERY_FULL;
    else
        return BATTERY_UNKNOWN;
}

static int battery_perc(const char *path) {
    FILE *fp = fopen(path, "r");
    int  ret = -1;
    if (fp == NULL) {
        warn_err("fopen '%s'", path);
        return ret;
    }
    if (fscanf(fp, "%3d", &ret) != 1) {
        warn("failed to parse '%s'", path);
        ret = -1;
    }
    fclose(fp);
    return ret;
}

static wstatus_t update(widget_t *self) {
    battery_data_t *data = (battery_data_t *)self->data;

    data->notchanged = false;

    int perc = battery_perc(data->path_capacity);
    if (perc < 0)
        return self->status = WIDGET_NA;
    enum bstatus_e status = battery_status(data->path_status);
    if (perc > 100) {
        perc   = 100;
        status = BATTERY_FULL;
    }

    if (data->perc == perc && data->status == status) {
        data->notchanged = true;
        return self->status = WIDGET_OK;
    }

    data->perc   = perc;
    data->status = status;
    char ready[6];
    if (status == BATTERY_FULL) {
        strncpy(ready, "full", sizeof(ready));
    }
    else {
        snprintf(ready, sizeof(ready), "%c%d%%", bstatus2ch[status], perc);
    }
    ready[5] = '\0';
    snprintf(self->result.buf, self->result.size, self->fmt, ready);
    self->result.buf[self->result.size-1] = '\0';

    return self->status = WIDGET_OK;
}

static void event_cb(evutil_socket_t fd, short what, void *arg) {
    widget_t       *self = (widget_t *)arg;
    battery_data_t *data = (battery_data_t *)self->data;
    update(self);
    if (data->notchanged)
        return;
    common_widget_update_st();
}

void battery_init(widget_t *self) {
    battery_args_t *args = (battery_args_t *)self->data;
    battery_data_t *data = self->data = malloc(sizeof(battery_data_t));
    if (data == NULL) {
        warn_err("malloc");
        return;
    }
    data->perc          = -1;
    data->status        = BATTERY_UNKNOWN;
    data->path_capacity = args->path_capacity;
    data->path_status   = args->path_status;
    struct timeval tv = {
        .tv_sec  = args->delay,
        .tv_usec = 0,
    };
    self->ev = common_event_new(-1, EV_PERSIST, event_cb, (void *)self);
    if (self->ev == NULL) {
        warn_err("event_new");
        return;
    }
    if (event_add(self->ev, &tv) != 0) {
        warn_err("event_add");
        return;
    }
    update(self);
}

void battery_cleanup(widget_t *self) {
    if (self->data == NULL)
        return;
    free(self->data);
}
