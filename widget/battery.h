#ifndef WIDGET_BATTERY_H
#define WIDGET_BATTERY_H

#include "common/widget.h"

struct battery_args_s {
    int delay;
    const char *path_capacity;
    const char *path_status;
};

typedef struct battery_args_s battery_args_t;

#define WIDGET_BATTERY(format, bufsize, battery_args) { \
        COMMON_WIDGET(bufsize),    \
        .init     = battery_init,     \
        .cleanup  = battery_cleanup,  \
        .fmt      = (format),      \
        .data     = (void *)(battery_args), \
    }

void battery_init(widget_t *self);
void battery_cleanup(widget_t *self);

#endif /* WIDGET_BATTERY_H */
