#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <event2/event.h>

#define ERROR_PREFIX "wdgt-cmd"
#include "common.h"

#include "widget/cmd.h"

#define BUFSIZE 512

typedef struct cmd_data_s {
    int            pipefd[2];
    pid_t          pid;
    bool           term_was_sent;
    struct event  *cmd_ev;
    char         **cmd;
    char          *saved_buf;
    char          *tmp_buf;
    struct timeval delay;
    struct timeval timeout;
} cmd_data_t;

static void clean_data(cmd_data_t *data) {
    if (data->cmd_ev != NULL) {
        event_free(data->cmd_ev);
        data->cmd_ev = NULL;
    }
    if (data->pipefd[0] >= 0) {
        close(data->pipefd[0]);
        data->pipefd[0] = -1;
    }
    if (data->pipefd[1] >= 0) {
        close(data->pipefd[1]);
        data->pipefd[1] = -1;
    }
    if (data->pid > 0) {
        int count = 0;
        /* try to waitpid 3 times with sending term and kill */
        while (count++ < 3) {
            pid_t ret = waitpid(data->pid, NULL, WNOHANG);
            if (ret == -1) {
                /* ECHILD means we have no such child */
                if (errno == ECHILD)
                    break;
                else
                    warn_err("waitpid");
            }
            else if (ret == data->pid) {
                break;
            }
            if (data->term_was_sent) {
                /* by some reason SIGTERM was ignored */
                warn("sending SIGKILL to pid %d", data->pid);
                kill(data->pid, SIGKILL);
            }
            else {
                kill(data->pid, SIGTERM);
                data->term_was_sent = true;

            }
            usleep(100);
        }
        data->pid = -1;
    }
    data->term_was_sent = false;
    data->saved_buf[0] = '\0';
}

static void saved_buf_add(widget_t *self, char *buf, size_t count) {
    cmd_data_t *data = (cmd_data_t *)self->data;
    if (count <= 0)
        return;
    size_t saved_len = strnlen(data->saved_buf, self->result.size);
    /* buffer fulled */
    if (saved_len >= self->result.size - 1)
        return;
    size_t n = self->result.size - 1 - saved_len;
    if (count < n)
        n = count;
    memcpy(data->saved_buf+saved_len, buf, n);
    data->saved_buf[saved_len + n] = '\0';
}

static void cmd_cb(evutil_socket_t fd, short what, void *arg) {
    widget_t   *self = (widget_t *)arg;
    cmd_data_t *data = (cmd_data_t *)self->data;
    if (what & EV_READ) {
        char buf[BUFSIZE];
        size_t count = read(fd, buf, BUFSIZE);
        /* some error while reading */
        if (count < 0) {
            /* EINTR - its ok, just do retry */
            if (errno == EINTR)
                return;
            /* otherwise kill child */
            warn_err("read");
            clean_data(data);
            if (event_add(self->ev, &data->delay) != 0)
                warn_err("event_add");
        }
        else if (count == 0) {
            /* waitpid and remove this event will be here */
            clean_data(data);
            if (event_add(self->ev, &data->delay) != 0)
                warn_err("event_add");
        }
        else {
            char *end = memrchr(buf, '\n', count);
            /* no end of line - save result to temperory buffer */
            if (end == NULL) {
                saved_buf_add(self, buf, count);
                return;
            }

            char *start = memrchr(buf, '\n', (end - buf));
            if (start == NULL)
                start = buf;
            else
                start += 1;
            saved_buf_add(self, start, end-start);
            /* update widget if text_changed */
            snprintf(
                data->tmp_buf, self->result.size, self->fmt, data->saved_buf
            );
            data->tmp_buf[self->result.size-1] = '\0';
            self->status = WIDGET_OK;
            if (strcmp(data->tmp_buf, self->result.buf) != 0) {
                strcpy(self->result.buf, data->tmp_buf);
                common_widget_update_st();
            }

            /* update saved buffer */
            data->saved_buf[0] = '\0';
            if ((end - buf + 1) < count) {
                start = end + 1;
                saved_buf_add(self, end + 1, count - (start - buf));
            }
        }
    }
    else if (what & EV_TIMEOUT) {
        /* read timeout - kill child. Do not wait ending here, after dying,
         * child will close pipe and we will get EV_READ */
        if (data->term_was_sent) {
            warn("sending SIGKILL to pid %d", data->pid);
            kill(data->pid, SIGKILL);
        }
        else {
            warn("sending SIGTERM to pid %d", data->pid);
            kill(data->pid, SIGTERM);
            data->term_was_sent = true;
        }
    }
}

static int run_cmd(widget_t *self) {
    cmd_data_t *data = (cmd_data_t *)self->data;
    if (pipe(data->pipefd) != 0) {
        warn_err("pipe");
        return -1;
    }

    data->cmd_ev = common_event_new(
        data->pipefd[0], EV_READ|EV_PERSIST, cmd_cb, (void *)self
    );
    if (data->cmd_ev == NULL) {
        warn_err("event_new");
        return -1;
    }

    struct timeval *tv =
      (data->timeout.tv_sec == 0 && data->timeout.tv_usec == 0)
        ? NULL
        : &data->timeout;
    if (event_add(data->cmd_ev, tv)) {
        warn_err("event_add");
        return -1;
    }

    data->pid = fork();
    if (data->pid < 0) {
        warn_err("fork");
        return -1;
    }
    else if (data->pid == 0) {
        /* close reader */
        close(data->pipefd[0]);
        if (dup2(data->pipefd[1], STDOUT_FILENO) != STDOUT_FILENO)
            die_err("dup2");
        execv(data->cmd[0], data->cmd);
        die_err("execv");
    }
    else {
        /* close writer */
        close(data->pipefd[1]);
        data->pipefd[1] = -1;
        return 0;
    }
}

/* on delay timeout run program or if it fails restart delay */
static void delay_cb(evutil_socket_t fd, short what, void *arg) {
    widget_t   *self = (widget_t *)arg;
    cmd_data_t *data = (cmd_data_t *)self->data;
    if (run_cmd(self) == 0)
        return;
    clean_data(data);
    if (event_add(self->ev, &data->delay) != 0)
        warn_err("event_add");
}


void cmd_init(widget_t *self) {
    cmd_args_t *args = (cmd_args_t *)self->data;
    cmd_data_t *data = self->data = calloc(1, sizeof(cmd_data_t));
    if (data == NULL) {
        warn_err("calloc");
        return;
    }

    data->pipefd[0] = -1;
    data->pipefd[1] = -1;
    data->pid       = -1;
    data->cmd_ev = NULL;
    data->cmd    = args->cmd;
    data->delay.tv_sec   = args->delay;
    data->timeout.tv_sec = args->timeout;
    data->tmp_buf = NULL;
    /* temperory buf for saving output between read cbs */
    data->saved_buf = malloc(self->result.size);
    if (data->saved_buf == NULL) {
        warn_err("malloc");
        return;
    }
    data->saved_buf[0] = '\0';

    data->tmp_buf = malloc(self->result.size);
    if (data->tmp_buf == NULL) {
        warn_err("malloc");
        return;
    }
    data->tmp_buf[0] = '\0';

    self->ev = common_event_new(-1, 0, delay_cb, (void *)self);
    if (self->ev == NULL) {
        warn_err("event_new");
        return;
    }
    /* call delay_cb as soon as posible */
    event_active(self->ev, EV_TIMEOUT, 1);
    self->status = WIDGET_NA;
}

void cmd_cleanup(widget_t *self) {
    if (self->data == NULL)
        return;
    cmd_data_t *data = (cmd_data_t *)self->data;
    clean_data(self->data);
    if (data->saved_buf != NULL)
        free(data->saved_buf);
    if (data->tmp_buf != NULL)
        free(data->tmp_buf);
    free(self->data);
}
