#ifndef WIDGET_CMD_H
#define WIDGET_CMD_H

#include <stdlib.h>

#include "common/widget.h"

#define NEW_CMD(...)       { __VA_ARGS__, NULL }
#define NEW_SHELL_CMD(cmd) { "/bin/sh", "-c", cmd, NULL }

struct cmd_args_s {
    int    delay;
    int    timeout;
    char **cmd;
};

typedef struct cmd_args_s cmd_args_t;

#define WIDGET_CMD(format, bufsize, cmd_args) { \
        COMMON_WIDGET(bufsize),    \
        .init     = cmd_init,      \
        .cleanup  = cmd_cleanup,   \
        .fmt      = (format),      \
        .data     = (void *)(cmd_args), \
    }

void cmd_init(widget_t *self);
void cmd_cleanup(widget_t *self);

#endif /* WIDGET_BATTERY_H */
