#include <time.h>
#include <signal.h>
#include <stdlib.h>
#include <stdbool.h>

#include <event2/event.h>

#define ERROR_PREFIX "wdgt-datetime"
#include "common.h"

typedef struct datetime_data_s {
    int     sig;
    bool    timer_created;
    timer_t timer;
} datetime_data_t;


static wstatus_t update(widget_t *self) {
    struct timespec tp;
    struct tm ltime;
    if (clock_gettime(CLOCK_REALTIME, &tp) != 0) {
        warn_err("clock_gettime");
        return self->status = WIDGET_NA;
    }
    localtime_r(&tp.tv_sec, &ltime);
    if (strftime(self->result.buf, self->result.size, self->fmt, &ltime) == 0) {
        warn("strftime: buffer overflow");
        return self->status = WIDGET_ERROR;
    }
    return self->status = WIDGET_OK;
}

static void event_cb(evutil_socket_t fd, short what, void *arg) {
    widget_t *self = (widget_t *)arg;
    update(self);
    common_widget_update_st();
}

void datetime_init(widget_t *self) {
    time_t delay = (time_t)self->data;

    datetime_data_t *data = self->data = calloc(1, sizeof(datetime_data_t));
    if (data == NULL) {
        warn_err("calloc");
        return;
    }

    data->sig = common_sigrt_new();
    if (data->sig < 0) {
        warn("no free RT signals left");
        return;
    }

    struct sigevent sev = {
        .sigev_notify = SIGEV_SIGNAL,
        .sigev_signo  = data->sig,
    };
    sev.sigev_value.sival_ptr = NULL;

    if (timer_create(CLOCK_REALTIME, &sev, &data->timer) != 0) {
        warn_err("timer_create");
        return;
    }
    data->timer_created = true;


    self->ev = common_sigevent_new(data->sig, event_cb, (void *)self);
    if (self->ev == NULL) {
        warn_err("sigevent_new");
        return;
    }
    if (event_add(self->ev, NULL) != 0) {
        warn_err("event_add");
        return;
    }

    struct itimerspec its;
    time_t ts;
    time(&ts);
    /* normalize timer start value */
    ts -= ts % delay;
    its.it_interval.tv_sec  = delay;
    its.it_interval.tv_nsec = 0;
    its.it_value.tv_sec     = ts;
    its.it_value.tv_nsec    = 0;
    if (timer_settime(data->timer, TIMER_ABSTIME, &its, NULL) != 0) {
        warn_err("timer_settime");
        return;
    }

    update(self);
}

void datetime_cleanup(widget_t *self) {
    if (self->data == NULL)
        return;
    datetime_data_t *data = (datetime_data_t *)self->data;
    if (data->timer_created)
        timer_delete(data->timer);
    if (data->sig > 0)
        common_sigrt_free(data->sig);
    free(self->data);
}


