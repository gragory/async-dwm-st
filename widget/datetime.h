#ifndef WIDGET_DATETIME_H
#define WIDGET_DATETIME_H

#include "common/widget.h"

#define WIDGET_DATETIME(format, bufsize, delay) { \
        COMMON_WIDGET(bufsize),   \
        .init     = datetime_init,    \
        .cleanup  = datetime_cleanup, \
        .fmt      = (format),         \
        .data     = (void *)(delay),  \
    }

void datetime_init(widget_t *self);
void datetime_cleanup(widget_t *self);

#endif /* WIDGET_DATETIME_H */
