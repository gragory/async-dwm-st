#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

#include <event2/event.h>
#include <libmnl/libmnl.h>
#include <linux/rtnetlink.h>
#include <linux/wireless.h>

#define ERROR_PREFIX "wdgt-essid"
#include "common.h"

typedef struct essid_data_s {
    const char        *ifname;
    char              *buf;
    struct mnl_socket *nl;
    struct event      *upev;
    bool               upev_added;
} essid_data_t;

static wstatus_t update(widget_t *self) {
    essid_data_t *data = (essid_data_t *)self->data;
	int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        warn_err("socket");
        return self->status = WIDGET_ERROR;
    }
	struct iwreq wreq;
	char id[IW_ESSID_MAX_SIZE+1];

	memset(&wreq, 0, sizeof(struct iwreq));
	wreq.u.essid.length  = IW_ESSID_MAX_SIZE+1;
	wreq.u.essid.pointer = id;
    strncpy(wreq.ifr_name, data->ifname, sizeof(wreq.ifr_name));
    wreq.ifr_name[sizeof(wreq.ifr_name) - 1] = '\0';

	if (ioctl(sockfd, SIOCGIWESSID, &wreq) == -1) {
        warn_err("ioctl");
        close(sockfd);
        return self->status = WIDGET_ERROR;
    }
    close(sockfd);

    const char *essid = (const char *)wreq.u.essid.pointer;

	if (strcmp(essid, "") == 0)
		return self->status = WIDGET_NA;

    snprintf(self->result.buf, self->result.size, self->fmt, essid);
    self->result.buf[self->result.size-1] = '\0';
    return self->status = WIDGET_OK;
}

static void update_cb(evutil_socket_t fd, short what, void *arg) {
    widget_t     *self = (widget_t *)arg;
    essid_data_t *data = (essid_data_t *)self->data;
    update(self);
    common_widget_update_st();
    data->upev_added = false;
}

static int data_attr_cb(const struct nlattr *attr, void *arg) {
    widget_t     *self = (widget_t *)arg;
    essid_data_t *data = (essid_data_t *)self->data;
	int           type = mnl_attr_get_type(attr);

	if (mnl_attr_type_valid(attr, IFLA_MAX) < 0)
		return MNL_CB_OK;

    if (type != IFLA_IFNAME)
        return MNL_CB_OK;

    if (mnl_attr_validate(attr, MNL_TYPE_STRING) < 0) {
        warn_err("mnl_attr_validate");
        return MNL_CB_ERROR;
    }

    if (strcmp(mnl_attr_get_str(attr), data->ifname) == 0) {
        if (!data->upev_added) {
            static const struct timeval tv = { .tv_sec = 0, .tv_usec = 300000 };
            if (event_add(data->upev, &tv) != 0)
                warn_err("event_add");
            else
                data->upev_added = true;
        }
    }

	return MNL_CB_OK;
}

static int data_cb(const struct nlmsghdr *nlh, void *arg) {
    struct ifinfomsg *ifm = mnl_nlmsg_get_payload(nlh);
    mnl_attr_parse(nlh, sizeof(*ifm), data_attr_cb, arg);
    return MNL_CB_OK;
}

static void event_cb(evutil_socket_t fd, short what, void *arg) {
    widget_t     *self = (widget_t *)arg;
    essid_data_t *data = (essid_data_t *)self->data;
    size_t ret = mnl_socket_recvfrom(data->nl, data->buf, MNL_SOCKET_BUFFER_SIZE);
    if (ret < 0) {
        /* EINTR - its ok, just do retry */
        if (errno != EINTR)
            warn_err("mnl_socket_recvfrom");
        return;
    }
    ret = mnl_cb_run(data->buf, ret, 0, 0, data_cb, (void *)self);
    if (ret <= 0)
        warn_err("mnl_cb_run");
}

void essid_init(widget_t *self) {
    const char *ifname = (const char *)self->data;
    essid_data_t *data = self->data = malloc(sizeof(essid_data_t));
    if (data == NULL) {
        warn_err("malloc");
        return;
    }
    data->buf        = NULL;
    data->nl         = NULL;
    data->upev       = NULL;
    data->ifname     = ifname;
    data->upev_added = false;

    data->buf = malloc(MNL_SOCKET_BUFFER_SIZE);
    if (data->buf == NULL) {
        warn_err("malloc");
        return;
    }

    data->upev = common_event_new(-1, 0, update_cb, (void *)self);
    if (data->upev == NULL) {
        warn_err("event_add");
        return;
    }

    data->nl = mnl_socket_open(NETLINK_ROUTE);
    if (data->nl == NULL) {
        warn_err("mnl_socket_open");
        return;
    }

    if (mnl_socket_bind(data->nl, RTMGRP_LINK, MNL_SOCKET_AUTOPID) < 0) {
        warn_err("mnl_socket_bind");
        return;
    }

    int fd = mnl_socket_get_fd(data->nl);
    self->ev = common_event_new(fd, EV_READ|EV_PERSIST, event_cb, (void *)self);
    if (self->ev == NULL) {
        warn_err("event_new");
        return;
    }
    if (event_add(self->ev, NULL) != 0) {
        warn_err("event_add");
        return;
    }

    update(self);
}

void essid_cleanup(widget_t *self) {
    if (self->data == NULL)
        return;
    essid_data_t *data = (essid_data_t *)self->data;
    if (data->nl != NULL)
        mnl_socket_close(data->nl);
    if (data->buf != NULL)
        free(data->buf);
    if (data->upev != NULL)
        event_free(data->upev);
    free(self->data);
}
