#ifndef WIDGET_ESSID_H
#define WIDGET_ESSID_H

#include "common/widget.h"

#define WIDGET_ESSID(format, bufsize, ifname) { \
        COMMON_WIDGET(bufsize),    \
        .init     = essid_init,      \
        .cleanup  = essid_cleanup,   \
        .fmt      = (format),      \
        .data     = (void *)(ifname), \
    }

void essid_init(widget_t *self);
void essid_cleanup(widget_t *self);

#endif /* WIDGET_BATTERY_H */
